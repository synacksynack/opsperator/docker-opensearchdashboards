SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=$(FRONTNAME)/opensearchdashboards

-include Makefile.cust

.PHONY: osearch
osearch:
	@@docker rm -f osearch || echo mkay
	@@if sysctl -a >/dev/null 2>&1; then \
	    if ! sysctl vm.max_map_count | grep 262144 >/dev/null; then \
		if test `id -u` = 0; then \
		    sysctl -w vm.max_map_count=262144; \
		else \
		    sudo sysctl -w vm.max_map_count=262144; \
		fi; \
	    fi; \
	else \
	    echo WARNING: if startup fails, sysctl -w vm.max_map_count=262144; \
	fi
	docker run --name osearch \
	    -p 9200:9200 \
	    -p 9300:9300 \
	    -e HOSTNAME=os-0 \
	    -d opsperator/opensearch
	@@sleep 10

.PHONY: test
test:
	@@if ! docker ps | grep osearch >/dev/null; then \
	    make osearch; \
	fi
	@@docker rm -f osdashboard || echo mkay
	@@osip=`docker inspect osearch | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	if test -z "$$osip"; then \
	    osip=`ip a show $$(ip r | awk '/default/{print $$5}') | awk '/ inet /{print $$2}' | awk -F/ '{print $$1}'`; \
	fi; \
	docker run --name osdashboard \
	    -p 5601:5601 \
	    -e DEBUG=yay \
	    -e OPENSEARCH_HOSTS=http://$$osip:9200/ \
	    -e OPENSEARCH_REQUESTTIMEOUT=30000 \
	    -e XPACK_SECURITY_ENABLED=false \
	    -it $(IMAGE)


.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "OPENSEARCH_DASHBOARDS_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "OPENSEARCH_DASHBOARDS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
