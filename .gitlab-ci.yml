image: registry.gitlab.com/gdunstone/docker-buildx-qemu

services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

stages:
  - build
  - test
  - release

variables:
  CI_BUILD_ARCHS: linux/arm64/v8,linux/amd64
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_TLS_CERTDIR: ""
  IMAGE_ADDR: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  IMAGE_CACHE: $CI_REGISTRY_IMAGE/cache:$CI_COMMIT_SHA
  IMAGE_FINAL: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

build:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
  script:
    - >
        docker buildx build --platform $CI_BUILD_ARCHS \
            --cache-from=type=registry,ref=$IMAGE_CACHE \
            --cache-to=type=registry,ref=$IMAGE_CACHE \
            --progress plain --pull --push \
            -t "$IMAGE_ADDR" .

test:
  stage: test
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  image: docker:19.03.12
  script:
    - sudo sysctl -w vm.max_map_count=262144 || echo wamp wamp
    - >
        docker run --name testos \
            -p 9200:9200 \
            -d registry.gitlab.com/synacksynack/opsperator/docker-opensearch:master
    - sleep 120
    - docker inspect testos
    - >
        docker inspect testos | awk '/"IPAddress": "/{print $2}'
    - >
        docker inspect testos | awk '/"IPAddress": "/{print $2}' | head -1
    - >
        docker inspect testos | awk '/"IPAddress": "/{print $2}' | head -1 | cut -d'"' -f2 || true
    - >
        osip=`docker inspect testos | awk '/"IPAddress": "/{print $2}' | head -1 | cut -d'"' -f2 || true`;
        echo "Working with opensearch=$osip";
        echo "OPENSEARCH_HOSTS=http://$osip:9200" >.env;
    - >
        docker run --name testosd \
          --env-file=./.env \
          -e OPENSEARCH_REQUESTTIMEOUT=30000 \
          -e XPACK_SECURITY_ENABLED=false \
          -d $IMAGE_ADDR
    - sleep 90
    - docker logs testos | tail -100 || true
    - >
        works=false;
        for try in 1 2 3 4 5;
        do
            echo "=== Test $try/5 ==="
            if docker exec testosd /bin/sh -c 'curl -fsL http://127.0.0.1:5601/app/home' 2>&1 | grep '<title>OpenSearch Dashboards</title>'; then
                works=true;
                break;
            elif test $try -eq 5; then
                break;
            fi;
            echo retrying in 10 seconds;
            sleep 10;
        done;
        echo "=== Container Logs ===";
        docker logs testosd;
        echo "=== Cleaning Up ===";
        docker rm -f testosd;
        docker rm -f testos;
        if $works; then
            echo "Test: OK, OpenSearch Dashboards starts and listens";
        else
            echo "Test: KO, OpenSearch Dashboards does not answer";
            exit 1;
        fi

scan:
  stage: test
  image:
    name: docker.io/aquasec/trivy
    entrypoint: [""]
  script:
    - trivy image "$IMAGE_ADDR" || echo too-bad

retag:
  stage: release
  image:
    name: quay.io/skopeo/stable
    entrypoint: [""]
  script:
    - >
        if test "$CI_COMMIT_TAG"; then
            skopeo copy --all \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$IMAGE_ADDR docker://$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
        else
            skopeo copy \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$IMAGE_ADDR docker://$IMAGE_FINAL
        fi

release-code:
  rules:
    - if: $CI_COMMIT_TAG
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: '$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
  script:
    - echo 'Releasing $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG'
