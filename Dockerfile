FROM docker.io/debian:buster-slim

ARG DO_UPGRADE=
ARG OSD_VERSION=2.2.0
ENV DEBIAN_FRONTEND=noninteractive \
    BASE_URL=https://artifacts.opensearch.org/releases/bundle/opensearch-dashboards \
    OPENSEARCH_DASHBOARDS_HOME=/usr/share/opensearch-dashboards \
    OSD_GID=1000 \
    OSD_UID=1000 \
    NOTOSANS_TTC=https://github.com/googlefonts/noto-cjk/raw/NotoSansV2.001/NotoSansCJK-Regular.ttc

LABEL io.k8s.description="OpenSearch Dashboards" \
      io.k8s.display-name="OpenSearch Dashboards $OSD_VERSION" \
      io.openshift.expose-services="5601:opensearchdashboards" \
      io.openshift.tags="opensearchdashboards" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-opensearchdashboards" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$OSD_VERSION"

COPY config/* /

RUN set -x \
    && mkdir -p $OPENSEARCH_DASHBOARDS_HOME /usr/share/fonts/local \
	/usr/share/man/man1 /usr/share/man/man7 \
    && echo "# Install Dumb-init" \
    && apt-get update -y \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install OpenSearch Dashboard Dependencies" \
    && apt-get install -y --no-install-recommends ca-certificates curl \
	wget libkrb5-dev git libfontconfig python krb5-config libssl-dev \
	libsasl2-dev libsasl2-modules-gssapi-mit gcc libc-dev make g++ \
	build-essential fontconfig libnss3 findutils x11-utils procps \
    && OSDARCH=`uname -m` \
    && if ! echo "$OSDARCH" | grep -E '(x86_64|aarch64)' \
	    >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    fi \
    && curl -k -L -o /usr/share/fonts/local/NotoSansCJK-Regular.ttc \
	$NOTOSANS_TTC \
    && fc-cache -v \
    && echo "# Install OpenSearch Dashboards" \
    && if echo "$OSDARCH" | grep aarch64 >/dev/null; then \
	curl -fsL -o /tmp/osd.tar.gz \
	    $BASE_URL/$OSD_VERSION/opensearch-dashboards-$OSD_VERSION-linux-arm64.tar.gz; \
    else \
	curl -fsL -o /tmp/osd.tar.gz \
	    $BASE_URL/$OSD_VERSION/opensearch-dashboards-$OSD_VERSION-linux-x64.tar.gz; \
    fi \
    && tar -xzf /tmp/osd.tar.gz -C $OPENSEARCH_DASHBOARDS_HOME --strip-components=1 \
    && mv /opensearch_dashboards.yml $OPENSEARCH_DASHBOARDS_HOME/config/ \
    && mv /docker-opensearch-dashboards /usr/local/bin/ \
    && mv $OPENSEARCH_DASHBOARDS_HOME/plugins \
	$OPENSEARCH_DASHBOARDS_HOME/plugins-ref \
    && mkdir -p OPENSEARCH_DASHBOARDS_HOME/plugins \
    && groupadd -r osd -g $OSD_GID \
    && useradd -r -s /usr/sbin/nologin -d $OPENSEARCH_DASHBOARDS_HOME \
	-c "OpenSearch Dashboards service user" -u $OSD_UID -g osd osd \
    && mkdir -p /var/log/osd \
    && echo "# Fixing permissions" \
    && chown -R osd:root $OPENSEARCH_DASHBOARDS_HOME /var/log/osd \
    && chmod -R g=u $OPENSEARCH_DASHBOARDS_HOME /var/log/osd \
    && find / -xdev -perm -4000 -exec chmod u-s {} \; \
    && echo "# Cleaning Up" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/man /tmp/osd.tar.gz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER $OSD_UID
WORKDIR $OPENSEARCH_DASHBOARDS_HOME
ENTRYPOINT ["dumb-init","--","/usr/local/bin/docker-opensearch-dashboards"]
